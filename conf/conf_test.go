// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package conf

import (
	"encoding/json"
	"testing"

	"gotest.tools/v3/assert"
)

func Test_Unmarshal(t *testing.T) {
	t.Run("SampleConf", func(t *testing.T) {
		payload := `
		{
			"gitlab_host": "https://gitlab.alpinelinux.org",
			"listen_on": 80,
			"tokens": {
				"personal_access_token": {
					"admin": "GIVE ME AN ADMIN PERSONAL ACCESS TOKEN",
					"user": "GIVE ME AN USER PERSONAL ACCESS TOKEN"
				},
				"oauth": {
					"admin": "GIVE ME AN ADMIN OAUTH TOKEN",
					"user": "GIVE ME AN USER OAUTH TOKEN"	
				}
			},
			"admin_routes": [{
				"path": "/api/v4/users",
				"method": "GET"
		  	}],
			"log_level": "info"
		}
		`

		var conf Options
		err := json.Unmarshal([]byte(payload), &conf)
		assert.NilError(t, err)

		assert.Equal(t, "https://gitlab.alpinelinux.org", conf.GitlabHost)
		assert.Equal(t, 80, conf.Port)
		assert.Equal(t, "GIVE ME AN ADMIN PERSONAL ACCESS TOKEN", conf.Tokens.PAC.Admin)
		assert.Equal(t, "GIVE ME AN USER PERSONAL ACCESS TOKEN", conf.Tokens.PAC.User)
		assert.Equal(t, "GIVE ME AN ADMIN OAUTH TOKEN", conf.Tokens.OAuth.Admin)
		assert.Equal(t, "GIVE ME AN USER OAUTH TOKEN", conf.Tokens.OAuth.User)
		assert.Equal(t, "/api/v4/users", conf.AdminRoutes[0].Path)
		assert.Equal(t, "GET", conf.AdminRoutes[0].Method)
		assert.Equal(t, "info", conf.LogLevel)
	})
}
