// SPDX-FileCopyrightText: 2020-2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package conf

type Options struct {
	GitlabHost  string  `json:"gitlab_host"` // Base URL of the GitLab instance we are talking t
	Port        int     `json:"listen_on"`   // Port that we will listen on for commands
	Tokens      Tokens  `json:"tokens"`
	AdminRoutes []Route `json:"admin_routes"`
	LogLevel    string  `json:"log_level"` // Configuration level that is used
}

type Tokens struct {
	PAC   PAC   `json:"personal_access_token"`
	OAuth OAuth `json:"oauth"`
}

type PAC struct {
	User  string `json:"user"`  // User API operations
	Admin string `json:"admin"` // Admin API operations
}

type OAuth struct {
	User  string `json:"user"`  // User API operations
	Admin string `json:"admin"` // Admin API operations
}

type Route struct {
	Path   string `json:"path"`
	Method string `json:"method"`
}
