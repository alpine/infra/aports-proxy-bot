FROM alpinelinux/golang:edge as builder

COPY --chown=build: . /home/build/src
WORKDIR /home/build/src

RUN go build

FROM alpine:edge

RUN adduser -D app && apk add --no-cache jq

COPY --from=builder --chown=app: /home/build/src/aports-proxy-bot /home/build/src/conf.tmpl.json /app/
COPY docker/entrypoint.sh /usr/local/bin/

USER app

WORKDIR /app

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
