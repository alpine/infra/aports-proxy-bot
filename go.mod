module gitlab.alpinelinux.org/Leo/aports-proxy-bot

go 1.18

require (
	github.com/boumenot/gocover-cobertura v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/rs/zerolog v1.26.1
	github.com/spf13/afero v1.8.2
	gotest.tools/v3 v3.0.3
)

require (
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
