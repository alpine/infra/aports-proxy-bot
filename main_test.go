// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/spf13/afero"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/skip"
)

const exampleConf string = `
{
	"gitlab_host": "https://gitlab.alpinelinux.org",
	"listen_on": 80,
	"tokens": {
		"personal_access_token": {
			"admin": "PersonalAdminToken",
			"user": "PersonalUserToken"
		},
		"oauth": {
			"admin": "OAuthAdminToken",
			"user": "OAuthUserToken"
		}
	},
	"log_level": "info"
}
`

func Test_main(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		t.Run("simple", func(t *testing.T) {
			// Replace our filesystem with a in-memory one
			fs = afero.NewMemMapFs()
			afs = &afero.Afero{Fs: fs}

			out := &bytes.Buffer{}
			log = zerolog.New(out)

			// Set it to a port we can use
			ourConf := strings.ReplaceAll(exampleConf, `"listen_on": 80`, `"listen_on": 8084`)

			// Write the configuration that will be read
			err := afs.WriteFile("conf.json", []byte(ourConf), os.FileMode(0o644))
			assert.NilError(t, err)

			retChan := make(chan int)
			go func() {
				retChan <- do()
			}()

			// retry the http request 3 times due to the usage of a goroutine which we
			// don't know when it is ready
			var (
				resp    *http.Response
				retries = 5
			)

			for retries > 0 {
				// Call the shutdown endpoint
				resp, err = http.Get("http://localhost:8084/shutdown")
				if err == nil {
					break
				}
				retries++
			}
			assert.NilError(t, err)
			assert.Equal(t, http.StatusOK, resp.StatusCode)

			ret := <-retChan
			assert.Equal(t, 0, ret)
			assert.Equal(t,
				`{"level":"info","log_level":"info","message":"set log level from configuration"}`+"\n",
				out.String(),
			)
		})
	})
	t.Run("fail", func(t *testing.T) {
		t.Run("noFile", func(t *testing.T) {
			// Replace our filesystem with a in-memory one
			fs = afero.NewMemMapFs()
			afs = &afero.Afero{Fs: fs}

			out := &bytes.Buffer{}
			log = zerolog.New(out)

			ret := do()
			assert.Equal(t, 1, ret)
			assert.Equal(
				t,
				`{"level":"error","error":"open conf.json: file does not exist","message":"failed to load configuration"}`+"\n",
				out.String(),
			)
		})
		t.Run("badLogLevel", func(t *testing.T) {
			// Replace our filesystem with a in-memory one
			fs = afero.NewMemMapFs()
			afs = &afero.Afero{Fs: fs}

			out := &bytes.Buffer{}
			log = zerolog.New(out)

			ourConf := strings.ReplaceAll(exampleConf, `"log_level": "info"`, `"log_level": "meme"`)

			// Write the configuration that will be read
			err := afs.WriteFile("conf.json", []byte(ourConf), os.FileMode(0o644))
			assert.NilError(t, err)

			ret := do()
			assert.Equal(t, 1, ret)
			assert.Equal(
				t,
				`{"level":"error","error":"Unknown Level String: 'meme', defaulting to NoLevel","log_level":"meme","message":"log_level in configuration is invalid"}`+"\n",
				out.String(),
			)
		})
		t.Run("badPort", func(t *testing.T) {
			skip.If(t, os.Getenv("GITLAB_CI") != "", `Gitlab CI lets us bind to a port that we should not be able to`)

			// Replace our filesystem with a in-memory one
			fs = afero.NewMemMapFs()
			afs = &afero.Afero{Fs: fs}

			out := &bytes.Buffer{}
			log = zerolog.New(out)

			// Write the configuration that will be read
			err := afs.WriteFile("conf.json", []byte(exampleConf), os.FileMode(0o644))
			assert.NilError(t, err)

			ret := do()
			assert.Equal(t, 1, ret)

			// We receive more than line of log output, so split it
			data := strings.Split(out.String(), "\n")
			assert.Equal(t,
				`{"level":"info","log_level":"info","message":"set log level from configuration"}`,
				data[0],
			)
			assert.Equal(t,
				`{"level":"error","error":"listen tcp :80: bind: permission denied","message":"failed to serve requests"}`,
				data[1],
			)
		})
		t.Run("badGitlabHost", func(t *testing.T) {})
		// error="parse \"https://123%45%6.org\": invalid URL escape \"%45\""

		// Replace our filesystem with a in-memory one
		fs = afero.NewMemMapFs()
		afs = &afero.Afero{Fs: fs}

		out := &bytes.Buffer{}
		log = zerolog.New(out)

		ourConf := strings.ReplaceAll(exampleConf, `"https://gitlab.alpinelinux.org"`, `"https://123%45%6.org"`)

		// Write the configuration that will be read
		err := afs.WriteFile("conf.json", []byte(ourConf), os.FileMode(0o644))
		assert.NilError(t, err)

		ret := do()
		assert.Equal(t, 1, ret)

		data := strings.Split(out.String(), "\n")
		assert.Equal(t,
			`{"level":"info","log_level":"info","message":"set log level from configuration"}`,
			data[0],
		)
		assert.Equal(t,
			`{"level":"error","error":"parse \"https://123%45%6.org\": invalid URL escape \"%45\"","message":"failed to serve requests"}`,
			data[1],
		)
	})
}

func Test_Configuration(t *testing.T) {
	t.Run("success/simple", func(t *testing.T) {
		// Replace our filesystem with a in-memory one
		fs = afero.NewMemMapFs()
		afs = &afero.Afero{Fs: fs}

		// Write the configuration that will be read
		err := afs.WriteFile("conf.json", []byte(exampleConf), os.FileMode(0o644))
		assert.NilError(t, err)

		// This will read from conf.json and fill a struct and return a pointer to it
		config, err := load()
		assert.NilError(t, err)

		assert.Equal(t, "https://gitlab.alpinelinux.org", config.GitlabHost)
		assert.Equal(t, 80, config.Port)
		assert.Equal(t, "PersonalAdminToken", config.Tokens.PAC.Admin)
		assert.Equal(t, "PersonalUserToken", config.Tokens.PAC.User)
		assert.Equal(t, "OAuthAdminToken", config.Tokens.OAuth.Admin)
		assert.Equal(t, "OAuthUserToken", config.Tokens.OAuth.User)
		assert.Equal(t, "info", config.LogLevel)
	})
	t.Run("fail/noFile", func(t *testing.T) {
		// Replace our filesystem with a in-memory one
		fs = afero.NewMemMapFs()
		afs = &afero.Afero{Fs: fs}

		_, err := load()
		assert.ErrorContains(t, err, os.ErrNotExist.Error())
	})
	t.Run("fail/empty", func(t *testing.T) {
		// Replace our filesystem with a in-memory one
		fs = afero.NewMemMapFs()
		afs = &afero.Afero{Fs: fs}

		afs.WriteFile("conf.json", []byte(""), os.FileMode(0o644))

		_, err := load()
		assert.ErrorType(t, err, &json.SyntaxError{})
		assert.ErrorContains(t, err, `unexpected end of JSON input`)
	})
}
