// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"encoding/json"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/spf13/afero"
	"gitlab.alpinelinux.org/Leo/aports-proxy-bot/conf"
	"gitlab.alpinelinux.org/Leo/aports-proxy-bot/proxy"
)

// This can be replaced in the main_test portion
var (
	fs  afero.Fs       = afero.NewOsFs()
	afs *afero.Afero   = &afero.Afero{Fs: fs}
	log zerolog.Logger = zerolog.New(os.Stdout).
		Output(
			zerolog.ConsoleWriter{
				Out:        os.Stdout,
				TimeFormat: time.RFC3339,
			}).
		With().
		Timestamp().
		Logger()
)

func main() { os.Exit(do()) }

func do() int {
	config, err := load()
	if err != nil {
		log.Error().
			Err(err).
			Msg("failed to load configuration")
		return 1
	}

	// Set the zerolog logging to this configuration level
	level, err := zerolog.ParseLevel(config.LogLevel)
	if err != nil {
		log.Error().
			Err(err).
			Str("log_level", config.LogLevel).
			Msg("log_level in configuration is invalid")
		return 1
	}
	zerolog.SetGlobalLevel(level)
	log.Info().
		Str("log_level", level.String()).
		Msg("set log level from configuration")

	err = proxy.Serve(config, &log)
	if err != nil {
		log.Error().
			Err(err).
			Msg("failed to serve requests")
		return 1
	}
	return 0
}

func load() (*conf.Options, error) {
	file, err := afs.ReadFile("conf.json")
	if err != nil {
		return nil, err
	}

	var config conf.Options

	err = json.Unmarshal(file, &config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
