#!/bin/sh

# shellcheck disable=SC3040
set -eu -o pipefail

jq \
  --arg PAT_ADMIN "${PAT_ADMIN-}" \
  --arg PAT_USER "${PAT_USER-}" \
  --arg OAUTH_ADMIN "${OAUTH_ADMIN-}" \
  --arg OAUTH_USER "${OAUTH_USER-}" \
  '. + {
  tokens: {
    personal_access_token: {
      admin: $PAT_ADMIN,
      user: $PAT_USER
    },
    oauth: {
      admin: $OAUTH_ADMIN,
      user: $OAUTH_USER
    }
  }
}' <conf.tmpl.json >conf.json

exec /app/aports-proxy-bot
