// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package proxy

// Export internal functions for testing
var (
	GenerateProxy = generateProxy
	Handle        = handle
)
