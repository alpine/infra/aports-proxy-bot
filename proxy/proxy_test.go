// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package proxy_test

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"gitlab.alpinelinux.org/Leo/aports-proxy-bot/conf"
	"gitlab.alpinelinux.org/Leo/aports-proxy-bot/proxy"
	"gotest.tools/v3/assert"
)

func nopLogger() *zerolog.Logger {
	l := zerolog.Nop()
	return &l
}

func Test_generateProxy(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		t.Parallel()
		_, err := proxy.GenerateProxy("https://gitlab.alpinelinux.org")
		assert.NilError(t, err)
	})
	t.Run("fail", func(t *testing.T) {
		t.Parallel()
		_, err := proxy.GenerateProxy("123%45%6")
		assert.Error(t, err, `parse "123%45%6": invalid URL escape "%6"`)
	})
}

func Test_Shutdown(t *testing.T) {
	t.Run("shutdown", func(t *testing.T) {
		config := conf.Options{
			GitlabHost: "https://gitlab.alpinelinux.org",
			Port:       8084,
			Tokens: conf.Tokens{
				PAC: conf.PAC{
					Admin: "",
					User:  "",
				},
				OAuth: conf.OAuth{
					Admin: "",
					User:  "",
				},
			},
			LogLevel: "info",
		}

		// retry the http request 3 times due to the usage of a goroutine which we
		// don't know when it is ready
		var (
			resp    *http.Response
			err     error
			retries = 5
		)

		errChan := make(chan error)
		go func() {
			errChan <- proxy.Serve(&config, nopLogger())
		}()

		for retries > 0 {
			// Call the shutdown endpoint
			resp, err = http.Get("http://localhost:8084/shutdown")
			if err == nil {
				break
			}
			retries++
		}
		assert.NilError(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		err = <-errChan
		assert.NilError(t, err)
	})
}

func Test_handle(t *testing.T) {
	t.Run("success/SimpleOAuth", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			// Print every header we received
			for key, values := range r.Header {
				for _, value := range values {
					fmt.Fprintf(w, "%s: %s\n", key, value)
				}
			}
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("TokenForOAuthOK", "", "TokenForOAuthOK", "", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("Authorization", "Bearer TokenForOAuthOK")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		data := string(body)

		assert.Assert(t, strings.Contains(data, "Authorization: Bearer TokenForOAuthOK"))
	})
	t.Run("success/SimplePersonal", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			// Print every header we received
			for key, values := range r.Header {
				for _, value := range values {
					fmt.Fprintf(w, "%s: %s\n", key, value)
				}
			}
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("", "TokenForPersonalOK", "", "TokenForPersonalOK", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("Private-Token", "TokenForPersonalOK")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		data := string(body)

		assert.Assert(t, strings.Contains(data, "Private-Token: TokenForPersonalOK"))
	})
	t.Run("success/SimplePriorityOAuth", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			// Print every header we received
			for key, values := range r.Header {
				for _, value := range values {
					fmt.Fprintf(w, "%s: %s\n", key, value)
				}
			}
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("TokenForOAuthOK", "TokenForPersonalOK", "TokenForOAuthOK", "TokenForPersonalOK", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("Authorization", "Bearer TokenForOAuthOK")
		req.Header.Add("Private-Token", "TokenForPersonalOK")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		data := string(body)

		assert.Assert(t, strings.Contains(data, "Authorization: Bearer TokenForOAuthOK"))
		assert.Assert(t, !strings.Contains(data, "Authorization: TokenForPersonalOK"))
	})
	t.Run("success/ReplacePersonalToken", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			// Print every header we received
			for key, values := range r.Header {
				for _, value := range values {
					fmt.Fprintf(w, "%s: %s\n", key, value)
				}
			}
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("", "TokenForPersonalOK", "", "ReplaceThisToken", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("Private-Token", "ReplaceThisToken")
		req.Header.Add("Authorization", "Bearer ReplaceThisToken")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		data := string(body)

		assert.Assert(t, strings.Contains(data, "Private-Token: TokenForPersonalOK"))
		assert.Assert(t, !strings.Contains(data, "Private-Token: ReplaceThisToken"))
		assert.Assert(t, !strings.Contains(data, "Authorization: Bearer ReplaceThisToken"))
	})
	t.Run("success/ReplaceOAuthToken", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			// Print every header we received
			for key, values := range r.Header {
				for _, value := range values {
					fmt.Fprintf(w, "%s: %s\n", key, value)
				}
			}
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("TokenForOAuthOK", "", "", "", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("Private-Token", "ReplaceThisToken")
		req.Header.Add("Authorization", "Bearer ReplaceThisToken")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusOK, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		data := string(body)

		assert.Assert(t, strings.Contains(data, "Authorization: Bearer TokenForOAuthOK"))
		assert.Assert(t, !strings.Contains(data, "Private-Token: ReplaceThisToken"))
		assert.Assert(t, !strings.Contains(data, "Authorization: Bearer ReplaceThisToken"))
	})
	t.Run("fail/ServerError", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusNotFound)
			io.Copy(w, strings.NewReader(`{"error":"404 Not Found"}`))
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("TokenForOK", "", "TokenForOK", "", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("Authorization", "Bearer TokenForOK")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusNotFound, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)

		data := struct {
			Message string `json:"error"`
		}{}

		err = json.Unmarshal(body, &data)
		assert.NilError(t, err)
		assert.Equal(t, "404 Not Found", data.Message)
	})
	t.Run("fail/OAuthNoMatch", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			// Print every header we received
			for key, values := range r.Header {
				for _, value := range values {
					fmt.Fprintf(w, "%s: %s\n", key, value)
				}
			}
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("TokenForOAuthOK", "", "StoredOAuthToken", "", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("Authorization", "Bearer ReplaceThisToken")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusForbidden, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		data := string(body)

		assert.Assert(t, strings.Contains(data, "Permission Denied - OAuth Token doesn't match"))
	})
	t.Run("fail/PACNoMatch", func(t *testing.T) {
		mainSrv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			// Print every header we received
			for key, values := range r.Header {
				for _, value := range values {
					fmt.Fprintf(w, "%s: %s\n", key, value)
				}
			}
		}))
		defer mainSrv.Close()

		revProxy, err := proxy.GenerateProxy(mainSrv.URL)
		assert.NilError(t, err)

		proxySrv := httptest.NewServer(http.HandlerFunc(proxy.Handle("", "TokenForPACOK", "", "StoredPACToken", revProxy, nopLogger())))
		defer proxySrv.Close()

		// Create a request
		req, _ := http.NewRequest("GET", proxySrv.URL, http.NoBody)
		req.Header.Add("private-token", "ReplaceThisToken")

		client := &http.Client{}

		resp, err := client.Do(req)
		assert.NilError(t, err)
		assert.Equal(t, http.StatusForbidden, resp.StatusCode)

		defer resp.Body.Close()

		body, _ := io.ReadAll(resp.Body)
		data := string(body)

		assert.Assert(t, strings.Contains(data, "Permission Denied - Personal Access Token doesn't match"))
	})
}
