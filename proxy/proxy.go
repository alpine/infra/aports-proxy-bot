// SPDX-FileCopyrightText: 2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package proxy

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"gitlab.alpinelinux.org/Leo/aports-proxy-bot/conf"
)

// userPath is the path where the user token will be used which has
// no permission to perform destructive actions, since it is not
// destructive then we match anything under the API prefix
const userPath string = "/api/v4/"

// generateProxy creates a proxy with a custom `director` that handles routes
// the request to the Gitlab instance URL as defined in the configuration
func generateProxy(gitlabURL string) (*httputil.ReverseProxy, error) {
	// Parse URL from configuration
	URL, err := url.Parse(gitlabURL)
	if err != nil {
		return nil, err
	}

	director := func(req *http.Request) {
		// Standard headers used by ReverseProxies
		req.Header.Add("X-Forwarded-Host", req.Host)
		req.Header.Add("X-Origin-Host", URL.Host)
		req.Host = URL.Host
		req.URL.Scheme = URL.Scheme
		req.URL.Host = URL.Host
	}
	return &httputil.ReverseProxy{Director: director}, nil
}

func handle(OutgoingOAuthToken, OutgoingPACToken, StoredOAuthToken, StoredPACToken string, proxy *httputil.ReverseProxy, log *zerolog.Logger) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var authMode string
		// 'Bearer + <TOKEN>', check it with the one we have stored
		IncomingOAuthToken := r.Header.Get("Authorization")

		if IncomingOAuthToken != "" {
			// We handle not having an StoredOAuthToken to make it easier to test
			if StoredOAuthToken != "" {
				if IncomingOAuthToken != "Bearer "+StoredOAuthToken {
					http.Error(w, "Permission Denied - OAuth Token doesn't match", http.StatusForbidden)
					return
				}
			}

			if OutgoingOAuthToken == StoredOAuthToken {
				authMode = "user"
			} else {
				authMode = "admin"
			}
		}

		IncomingPACToken := r.Header.Get("private-token")

		if IncomingPACToken != "" {
			// We handle not having an StoredPACToken to make it easier to test
			if StoredPACToken != "" {
				if IncomingPACToken != StoredPACToken {
					http.Error(w, "Permission Denied - Personal Access Token doesn't match", http.StatusForbidden)
					return
				}
			}

			if OutgoingPACToken == StoredPACToken {
				authMode = "user"
			} else {
				authMode = "admin"
			}
		}

		if IncomingOAuthToken == "" && IncomingPACToken == "" {
			http.Error(w, "Permission Denied - Authentication missing", http.StatusForbidden)
			return
		}

		log.Info().
			Str("Url", r.RequestURI).
			Str("AuthMode", authMode).
			Msg("Proxying request")

		// Remove these as we replace them with only one
		r.Header.Del("private-token")
		r.Header.Del("authorization")

		if OutgoingOAuthToken != "" {
			log.Debug().Msg("Setting authorization header")
			r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", OutgoingOAuthToken))
		} else if OutgoingPACToken != "" {
			log.Debug().Msg("Setting private-token header")
			r.Header.Set("private-token", OutgoingPACToken)
		}
		proxy.ServeHTTP(w, r)
	}
}

// Serve takes a struct representing the user configuration and sets up
// a proxy server with basic authenttication
func Serve(config *conf.Options, log *zerolog.Logger) error {
	// Create our little proxy with our custom director
	proxy, err := generateProxy(config.GitlabHost)
	if err != nil {
		return err
	}

	// Create a background context and defer its cancellation
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Create a new router from Gorilla toolkit
	router := mux.NewRouter()

	// Add each of the paths that are defined in the configuration to be handled by the admin
	for _, route := range config.AdminRoutes {
		log.Info().
			Str("path", route.Path).
			Str("method", route.Method).
			Msg("Adding handler for route")
		router.HandleFunc(
			route.Path,
			handle(
				config.Tokens.OAuth.Admin,
				config.Tokens.PAC.Admin,
				config.Tokens.OAuth.User,
				config.Tokens.PAC.User,
				proxy,
				log,
			),
		).Methods(route.Method)
	}

	// register `/shutdown` as a path to shutdown the server that just returns 200
	// and cancels the context
	router.HandleFunc("/shutdown", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		cancel()
	})

	// Route everything under /api/v4/ to the user function
	router.PathPrefix(userPath).HandlerFunc(
		handle(
			config.Tokens.OAuth.User,
			config.Tokens.PAC.User,
			config.Tokens.OAuth.User,
			config.Tokens.PAC.User,
			proxy,
			log,
		),
	)

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", config.Port),
		Handler: router,
	}

	// Channel for errors, we use it to receive errors from running the server
	errChan := make(chan error)

	// Run the server in a goroutine, use the error channel to return any errors
	// from the server
	go func() {
		// Listen until the end of times
		srvErr := srv.ListenAndServe()

		// Don't return anything if our error is because the server is closed
		if errors.Is(srvErr, http.ErrServerClosed) {
			errChan <- nil
		}
		errChan <- srvErr
	}()

	// Wait on the context channel which is triggered whenever `cancel()` is called
	// cancel is called whenever someone hits the `/shutdown` endpoint.
	// Also wait on the error channel, this is hit if the server has an error, and
	// return it
	select {
	case err = <-errChan:
		// Don't do anything, the assignment is enough, we wil return the error later
	case <-ctx.Done():
		// Shutdown the server when requested, check if thhe error we got is not
		// the context canceled error
		err = srv.Shutdown(ctx)
		if err != nil && errors.Is(err, context.Canceled) {
			err = nil
		}
	}
	return err
}
