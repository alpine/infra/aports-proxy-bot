# aports-proxy-bot

[![pipeline status](https://gitlab.alpinelinux.org/Leo/aports-proxy-bot/badges/master/pipeline.svg)](https://gitlab.alpinelinux.org/Leo/aports-proxy-bot/-/commits/master) [![coverage report](https://gitlab.alpinelinux.org/Leo/aports-proxy-bot/badges/master/coverage.svg)](https://gitlab.alpinelinux.org/Leo/aports-proxy-bot/-/commits/master) 

Small daemon to filter and proxy GitLab API calls from [aports-qa-bot](https://gitlab.alpinelinux.org/Cogitri/aports-qa-bot)

## Motivations and Purposes

- Proxy transparently so **aports-qa-bot** can continue using its the amazing [go-gitlab](https://github.com/xanzy/go-gitlab) library instead of having to deal with wrappers
- Protect from misuse of the API and permissions by only allowing administrator privileges on known good paths that are used by services from **aports-qa-bot** that have been vetted (everything else has permissions of an average user)

## Workings

**aports-proxy-bot** listens on the port defined by the **listen_on** key in its configuration and will wait on certain paths that are hardcoded, and change to meet the demands of the services from **aports-qa-bot**.

whenever a request is made in the port that **aports-proxy-bot** is listening on it will answer it, first by checking the path of the request, and depending on it it will use either of the 4 following tokens from its configuration:

- **tokens.oauth.admin**: used for API calls that require administrator permissions.
- **tokens.oauth.user**: used for API calls that can be made as an average user.
- **tokens.personal_access_token.admin**: same as **tokens.oauth.admin** used if the latter is not available.
- **tokens.personal_access_token.user**: same as **tokens.oauth.user** used if the latter is not available.

the chosen token between the 4 will be used by **aports-proxy-bot** first by removing the **Private-Token** and **Authorization** headers and replacing them with either:

- For OAuth tokens: the **Authorization** header with the value **Bearer `<TOKEN>`** where `<TOKEN>` is the token that was picked.
- For Personal Access Tokens: the **Private-Token** header with the value **`<TOKEN>`** where `<TOKEN>` is the token that was picked.

Finally the request is proxied to the GitLab instance as defined in **gitlab_host** the same way a normal reverse proxy works, the results of the proxy are then returned to the caller.

## Futures

Some things it would be nice to have:

- Expand this README to include **## Usage**

## Author

**maxice8/Leo <thinkabit.ukim@gmail.com>**

## License

This project as a whole is under the [GNU Affero General Public License v3.0 or later](https://www.gnu.org/licenses/agpl-3.0.en.html), its SPDX identifier is **AGPL-3.0-or-later**.

The license in full is included in [COPYING](./COPYING)

Certain parts might be under other licenses check the **SPDX-License-Identifier** header in the source code files for more information.
